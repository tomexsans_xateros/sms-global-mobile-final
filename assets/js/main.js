$(document).ready(function(){

    /*CHeck height and apply to class with 'js-auto-height'*/
  (function($,window,document){

        /*on load*/
        var windowHeight = $(window).height();
        $('.js-auto-height').css('min-height', windowHeight+'px');
        /*resized*/
        $(window).on('resize',function(){
            var windowHeight = $(window).height();
            
            $('.js-auto-height').css('min-height', windowHeight+'px');
        });
        // //console.log(windowHeight);
    })(jQuery,window,document,undefined)


    $('#dynamicYear').text( (new Date).getFullYear()  );
});

/*
    Anonymous functions
*/

(function($,window,document){

    function initHeaderNav(){
        $("#sidemenu-open").click(function(e) {
            e.preventDefault();
            $(this).parent('li').toggleClass('selected');
            $("#wrapper").toggleClass("toggled");
        });


        $("#search-open").click(function(e) {
            e.preventDefault();
            $(this).parent('li').toggleClass('selected');
            $(".search-container").toggleClass("show-searchbar");
        });
    }

    function initSidemenuLinks(){

        $('.haschild-menu').on('click',function(e){
            e.preventDefault();
            var elem = $(this);

            elem.parent('li').toggleClass('menu-open');
            // elem.next('ul').slideToggle();
        });
    }


    //call all functions when document is ready
    $(document).ready(function(){
        initHeaderNav();
        initSidemenuLinks();
    });


})(jQuery,window,document,undefined);


/*tooltip*/
$(function () {
  if($('[data-toggle="tooltip"]') !== "undefined"){
      $('[data-toggle="tooltip"]').tooltip();
  }
});

/*Chat Accordion Script*/
(function($,window,document){
    function initAccordion()
    {
        if($('.chat-accordion-panel__body') !== "undefined")
        {
            $('.chat-accordion-panel__body').on('show.bs.collapse', function () {
                    var elem = $(this);
                    elem.siblings('.chat-accordion-panel__body')
                             .find('.chat-accordion-panel__body-content.in')
                             .removeClass('in');
                });
        }
    }
        initAccordion();
})(jQuery,window,document,undefined);



/*+-----------------------------+
  | Upload File MAIN FUNCTION
  +-----------------------------+*/
(function($,window,document){
    //function for reading the file size
    //used to compute the total file size of the uploaded files
    function formatBytes(bytes,decimals) {
       if(bytes == 0) return '0.0 MB';
       var k = 1000;
       var dm = decimals + 1 || 3;
       var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
       var i = Math.floor(Math.log(bytes) / Math.log(k));
       return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
    }

    //function to iterate object and sum the size
    //small function that iterates and sum all uploaded file sizes
    function getSize(obj){
      var theSize = 0;
      $.each(obj, function(index, val) {
         // iterate through array or object 
         theSize += parseInt(val.size);
      });

      return formatBytes(theSize,2);
    }
  
    //url where to upload the file
    var urlFormAction = 'upload.php';

    //create our upload instance
    //http://www.dropzonejs.com/#configuration
    var FIleUploadInstance1 = new Dropzone('div#uploadviews',{
         //url where to upload the file
        url: urlFormAction,
        //our trigger to open the file upload
        clickable:['a#fileUploadButtonControl'],
        //stop auto uploading of files automatically
        autoProcessQueue:false,
        //process 2 files per upload
        parallelUploads:2,
        //max sizes per files in MB
        maxFilesize:3,
        //number of files allowed to upload per session
        maxFiles:1,
    });

    //event when a file is added
    FIleUploadInstance1.on("addedfile", function(file) {
      console.log(file);

      //get all attachments or files that are uploaded
      var count = FIleUploadInstance1.files.length; //read the length/count
      var size =  FIleUploadInstance1.files; //read the size
      
      //post number of uploaded files
      $('#dz_count_file').text(count);
      //use getSize function to iterate object and return total size
      $('#dz_size_file').text(getSize(size));

      //add a hidden field to the preview so when the form is submitted.
      //the hidden elements will also be submitted
      $("<input type='hidden'>").attr({
        name: 'images[]'
      }).val(file.name).appendTo(file.previewElement);

      //add event listener for catching delete button for the uploaded files
      file.previewElement.children[5].addEventListener("click", function() {
        FIleUploadInstance1.removeFile(file);
        //get all attachments or files that are uploaded
        var count = FIleUploadInstance1.files.length; //read the length/count
        var size =  FIleUploadInstance1.files; //read the size

        //post number of uploaded files
        $('#dz_count_file').text(count);
        //use getSize function to iterate object and return total size
        $('#dz_size_file').text(getSize(size));
      });   
    });

    // Hide the total progress bar when nothing's uploading anymore
    FIleUploadInstance1.on("queuecomplete", function(progress) {
      $(".dz-progress .progress").css('opacity', 0);
    });

    //show the loading/progres bar when process has started
    FIleUploadInstance1.on("process", function(progress) {
      $(".dz-progress .progress").css('opacity', 1);
    });


    //hide show attachments
    //the caret down that hides the preview container
    $('#dz-hide-show').on('click',function(event) {
        event.preventDefault();
        var pointer =  $(this).find('.fa');
        
        $('#uploadviews').slideToggle('slow',function(){
          // //console.log($('#uploadviews').is(":visible"));
            if($('#uploadviews').is(":visible")){
              $(pointer).removeClass('fa-chevron-right').addClass('fa-chevron-down');
            }else{
              $(pointer).removeClass('fa-chevron-down').addClass('fa-chevron-right');
            }
        });
    });

    /*+=================================
      | EMAIL FORM SUBMIT EVENT LISTENER
      +=================================*/
    //when submit button is clicked
    $('#submitForm').on('click',function(e){
      e.preventDefault();

      //serialize all post from the existing form
      var postData = $('#new-email').serialize();
      
      //process first the uploaded files
      FIleUploadInstance1.processQueue();
      // console.log(postData);

      //if file upload has been completed or is success
      FIleUploadInstance1.on("complete", function(file) {
        /*When File upload is complete remove the files*/
        // FIleUploadInstance1.removeFile(file);

        /*When File upload is complete submit the Form*/

        /*
        $.ajax({
          url: '/path/to/file',
          type: 'default GET (Other values: POST)',
          dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
          data: {param1: 'value1'},
        })
        .done(function() {
          console.log("success");
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });
        */
      });

      //if an error occured on the file upload
      FIleUploadInstance1.on("error", function(file) {
        
      });
    });

})(jQuery,window,document,undefined);