( function( $ ) {
 /*** 
  * Run this code when the #sidemenu-open link has been tapped
  * or clicked
  */
 $( '#sidemenu-open' ).on( 'touchstart click ', function(e) {
  e.preventDefault();

  $(this).parent('li').toggleClass('nav-open--active');
  var $body = $( 'body' ),
      $page = $( '#page-content-wrapper' ),
      $menu = $( '#sidebar-wrapper' ),
 
      /* Cross browser support for CSS "transition end" event */
      transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';

  /* When the toggle menu link is clicked, animation starts */
  if ( $body.hasClass( 'animating' ) ) {
    /*check if animation is in progress do nothing*/
  }else{
    $body.addClass( 'animating' );
    /***
     * Determine the direction of the animation and
     * add the correct direction class depending
     * on whether the menu was already visible.
     */
    if ( $body.hasClass( 'menu-visible' ) ) {
     $body.addClass( 'right' );
    } else {
     $body.addClass( 'left' );
    }
    
    /***
     * When the animation (technically a CSS transition)
     * has finished, remove all animating classes and
     * either add or remove the "menu-visible" class 
     * depending whether it was visible or not previously.
     */
    $page.on( transitionEnd, function() {
     $body
      .removeClass( 'animating left right' )
      .toggleClass( 'menu-visible' );

     $page.off( transitionEnd );
    } );
  }
 } );
} )( jQuery );


$(function(){
  // Bind the swipeleftHandler callback function to the swipe event on div.box
  if($('#sidebar-wrapper').length >= 1){
    $( "body" ).on( "swiperight", swipeHandler );
    $( "body" ).on( "swipeleft", swipeHandlerLeft );
    console.log('yes');
  }else{
    console.log('no');
  }
  // Callback function references the event target and adds the 'swipeleft' class to it
  function swipeHandler( event )
  {
    var $body = $( 'body' ),
        $page = $( '#page-content-wrapper' ),
        $menu = $( '#sidebar-wrapper' ),
      /* Cross browser support for CSS "transition end" event */
      transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';

      if($body.hasClass( 'animating' ) || $body.hasClass( 'menu-visible' )){
        event.preventDefault();
      }else{
        /* When the toggle menu link is clicked, animation starts */
        if ( $body.hasClass( 'menu-visible' ) || $body.hasClass( 'animating' )) {
          /*check if animation is in progress do nothing*/
        }else{
          $body.addClass( 'animating' );
        }
        /***
         * Determine the direction of the animation and
         * add the correct direction class depending
         * on whether the menu was already visible.
         */
        if ( $body.hasClass( 'menu-visible' ) ) {
         
        }else{
          $body.addClass( 'left' );
        }
        /***
         * When the animation (technically a CSS transition)
         * has finished, remove all animating classes and
         * either add or remove the "menu-visible" class 
         * depending whether it was visible or not previously.
         */
        $page.on( transitionEnd, function() {
         $body
          .removeClass( 'animating left right' )
          .toggleClass( 'menu-visible' );

         $page.off( transitionEnd );
        } );
      }
  }

  function swipeHandlerLeft( event )
  {
    var $body = $( 'body' ),
        $page = $( '#page-content-wrapper' ),
        $menu = $( '#sidebar-wrapper' ),
        /* Cross browser support for CSS "transition end" event */
        transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';


      if($body.hasClass( 'animating' )){
        /*check if animation is in progress do nothing*/
      }else{
        /* When the toggle menu link is clicked, animation starts */
        if ( $body.hasClass( 'menu-visible' ) ) {
          $body.addClass( 'animating' );
        }else{
          
        }
        /***
         * Determine the direction of the animation and
         * add the correct direction class depending
         * on whether the menu was already visible.
         */
        if ( $body.hasClass( 'menu-visible' ) && !$body.hasClass( 'right' )) {
          $body.addClass( 'right' );
        }else{
         
        }
        /***
         * When the animation (technically a CSS transition)
         * has finished, remove all animating classes and
         * either add or remove the "menu-visible" class 
         * depending whether it was visible or not previously.
         */
        $page.on( transitionEnd, function() {
         $body
          .removeClass( 'animating left right' )
          .toggleClass( 'menu-visible' );

         $page.off( transitionEnd );
        } );
      }
  }
});